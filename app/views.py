# import json
#
# from rest_framework.decorators import api_view
# from rest_framework.response import Response
# from rest_framework import status
#
# from app import models
# from app import serializers


'''
    方法视图
'''
# @api_view(['GET', 'POST']) # 只接受get和post请求
# def publisher_list(request, format=None):
#     '''
#     通过不同的方法，列出所有的出版社对象
#     或者创建一个出版社
#     :param request: 请求
#     :return: 出版社对象
#     '''
#     if request.method == 'GET':
#         '''
#             展示出版社信息
#         '''
#         queryset = models.Publisher.objects.all()
#         serializer = serializers.PublisherSerializer(queryset, many=True) # 多个遍历
#         data = serializer.data # OrderedDict对象
#         return Response(json.dumps(data), content_type='application/json', status=status.HTTP_200_OK)
#     if request.method == 'POST':
#         '''
#             创建出版社
#         '''
#         s = serializers.PublisherSerializer(date=request.date)
#         if s.is_valid(): # 如果数据符合规则
#             s.save()
#             return Response(s.data, status=status.HTTP_201_CREATED)
#         return Response(s.errors, status=status.HTTP_400_BAD_REQUEST)
#
#
# @api_view(['GET', 'PUT', 'DELETE'])
# def publisher_detail(request, pk, format=None):
#     '''
#         单个出版社对象的操作
#         查看、更新、删除
#     '''
#     try:
#         publisher = models.Publisher.objects.get(id=pk)
#     except models.Publisher.DoesNotExist:
#         return Response(status=status.HTTP_404_NOT_FOUND)
#
#     if request.method == 'GET':
#         serializer = serializers.PublisherSerializer(publisher)
#         return Response(serializer.data, status=status.HTTP_200_OK)
#
#     if request.method == 'PUT':
#         serializer = serializers.PublisherSerializer(publisher,
#                                                      data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#         return Response(status=status.HTTP_400_BAD_REQUEST)
#
#     if request.method == 'DELETE':
#         publisher.delete()
#         return Response(status=status.HTTP_204_NO_CONTENT)


'''
    类视图
'''
# from rest_framework.views import APIView
# from app import models, serializers
# from rest_framework.response import Response
# from django.http import Http404
# from rest_framework import status
#
#
# class PublisherList(APIView):
#     '''
#     列出所有的出版社，或者创建一个新的出版社
#     '''
#     def get(self, request, format=None):
#         publishers = models.Publisher.objects.all() # 查询出所有的出版社
#         serializer = serializers.PublisherSerializer(publishers, many=True)
#         return Response(serializer.data)
#
#     def post(self, request, format=None):
#         serializer = serializers.PublisherSerializer(data=request.data)
#         if serializer.is_valid(): # 如果提交的数据合法
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
#
#
# class PublisherDetail(APIView):
#     '''
#     具体的出版社，查看，修改，删除视图
#     '''
#     def get_object(self, pk):
#         try:
#             return models.Publisher.objects.get(pk=pk)
#         except models.Publisher.DoesNotExist:
#             raise Http404
#
#     def get(self, request, pk, format=None):
#         publisher = self.get_object(pk)
#         serializer = serializers.PublisherSerializer(publisher)
#         return Response(serializer.data)
#
#     def put(self, request, pk, format=None):
#         publisher = self.get_object(pk)
#         serializer = serializers.PublisherSerializer(publisher, data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
#
#     def delete(self, request, pk, format=None):
#         publisher = self.get_object(pk)
#         publisher.delete()
#         return Response(status=status.HTTP_204_NO_CONTENT)



'''
使用基于类视图的最大优势之一是它可以轻松地创建可复用的行为。
到目前为止，我们使用的创建/获取/更新/删除操作和我们创建的任何基于模型的API视图非常相似。
这些常见的行为是在REST框架的mixin类中实现的。
让我们来看看我们是如何通过使用mixin类编写视图的。

混合方法
'''
# from rest_framework import mixins, generics
#
# from app import models, serializers
#
#
# class PublisherList(mixins.ListModelMixin, # get请求
#                     mixins.CreateModelMixin, # post请求
#                     generics.GenericAPIView, # 通用视图
#                     ):
#     queryset = models.Publisher.objects.all() # 变量名不能变，固定写法
#     serializer_class = serializers.PublisherSerializer # 变量名不能变，固定写法
#
#     def get(self, request, *args, **kwargs):
#         return self.list(request, *args, **kwargs)
#
#     def post(self, request, *args, **kwargs):
#         return self.create(request, *args, **kwargs)
#
#
# class PublisherDetail(mixins.RetrieveModelMixin, # get请求，获取某一个
#                       mixins.UpdateModelMixin, # put请求
#                       mixins.DestroyModelMixin, # delete请求
#                       generics.GenericAPIView): # 通用试图请求
#     queryset = models.Publisher.objects.all() # 变量名不能变，固定写法
#     serializer_class = serializers.PublisherSerializer # 变量名不能变，固定写法
#
#     def get(self, request, *args, **kwargs):
#         return self.retrieve(request, *args, **kwargs)
#
#     def put(self, request, *args, **kwargs):
#         return self.update(request, *args, **kwargs)
#
#     def delete(self, request, *args, **kwargs):
#         return self.destroy(request, *args, **kwargs)

'''
    非常相似。这一次我们使用GenericAPIView类来提供核心功能，
    并添加mixins来提供.retrieve()），.update()和.destroy()操作。

使用通用的基于类的视图
通过使用mixin类，我们使用更少的代码重写了这些视图，但我们还可以再进一步。
REST框架提供了一组已经混合好（mixed-in）的通用视图
'''
from app import models, serializers
from rest_framework import generics


class PublisherList(generics.ListCreateAPIView):
    '''
    列出所有的出版社，或者创建一个新的出版社
    '''
    queryset = models.Publisher.objects.all() # 变量名不能变，固定写法
    serializer_class = serializers.PublisherSerializer # 变量名不能变，固定写法


class PublisherDetail(generics.RetrieveUpdateDestroyAPIView):
    '''
    具体的出版社，查看，修改，删除视图
    '''
    queryset = models.Publisher.objects.all()  # 变量名不能变，固定写法
    serializer_class = serializers.PublisherSerializer  # 变量名不能变，固定写法



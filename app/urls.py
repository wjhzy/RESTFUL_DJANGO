# -*- coding: utf-8 -*-
# File Name：     urls
# Author :        wjh
# date：          2019/5/10

from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from app import views

app_name = 'app'

urlpatterns = [
    # path('publisher/', views.publisher_list, name='publisher_list'),
    # path('publisher/<int:pk>/', views.publisher_detail,name='publisher_detail'),
    path('publisher/', views.PublisherList.as_view(), name='publisher_list'),
    path('publisher/<int:pk>/', views.PublisherDetail.as_view(),
         name='publisher_detail')
]
urlpatterns = format_suffix_patterns(urlpatterns)
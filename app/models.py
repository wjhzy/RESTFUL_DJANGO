from django.db import models


class Publisher(models.Model):
    name = models.CharField(max_length=32, verbose_name='名称', unique=True)
    address = models.CharField(max_length=128, verbose_name='地址')

    
    def __str__(self):
        return self.name
    
    class Meta:
        db_table = 'db_publisher'
        verbose_name = "出版商"
        verbose_name_plural = verbose_name
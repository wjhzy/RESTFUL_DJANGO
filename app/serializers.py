# -*- coding: utf-8 -*-
# File Name：     serializers
# Author :        wjh
# date：          2019/5/10

from rest_framework import serializers
from app import models

# class PublisherSerializer(serializers.Serializer):
#     id = serializers.IntegerField(read_only=True)
#     name = serializers.CharField(max_length=32)
#     address = serializers.CharField(max_length=128)
#
#     def create(self, validated_data):
#         '''
#         重写create方法，创建数据
#         :param validated_data: 用户传递的，经过了校验的数据
#         :return: 创建的数据
#         '''
#         return models.Publisher.objects.create(**validated_data)
#
#     def update(self, instance, validated_data):
#         '''
#         更新数据操作
#         :param instance: 原来的数据
#         :param validated_data: 用户传递的，经过了校验的数据
#         :return: 更新后的数据
#         '''
#         instance.name = validated_data.get('name', instance.name)
#         instance.address = validated_data.get('address', instance.address)
#         instance.save()
#         return instance


class PublisherSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Publisher
        fields = (
            'id',
            'name',
            'address'
        )